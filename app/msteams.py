import logging
import pymsteams


def alert(event, config, thisHost, timestamp):
    logger = logging.getLogger("main")

    myts = pymsteams.connectorcard(config["integrations"]["msteams"]["url"])
    messageSection = pymsteams.cardsection()
    messageSection.activityTitle(f"Type: {event['Type']}")
    messageSection.activitySubtitle(f"From : {thisHost}")
    messageSection.addFact("Timestamp", timestamp)
    messageSection.addFact("Action", event["Action"])
    messageSection.addFact("ID", event["Actor"]["ID"])
    ## Append name to payload if exists
    if "name" in event["Actor"]["Attributes"]:
        messageSection.addFact("Name: ", event["Actor"]["Attributes"]["name"])
    ## Append tags to payload if exists
    if "tags" in config["settings"]:
        tags = ", ".join([str(x) for x in config["settings"]["tags"]])
        messageSection.addFact("Tags: ", tags)

    myts.addSection(messageSection)
    myts.text("Docker Event Monitor")
    try:
        myts.send()
    except pymsteams.TeamsWebhookException as e:
        logger.error("{}: {}".format(__name__, e))
