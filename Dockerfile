FROM python:3-alpine
RUN apk update && apk upgrade \
    && pip install pyYaml docker six pymsteams
WORKDIR /app
COPY /app/* /app/
CMD python main.py